﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Server.Exceptions
{
    public class DtoValidationException : Exception
    {
        public DtoValidationException()
        {

        }

        public DtoValidationException(IList<ValidationFailure> failedValidation) : base($"Validation Failures ({failedValidation.Count}): \n {String.Join("\n", failedValidation)}")
        {

        }

    }


}
