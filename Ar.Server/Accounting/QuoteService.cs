﻿using Ar.DAL.Repos.Contracts;
using Ar.Domain.Models.Enterprise;
using Ar.Server.Contracts;
using Ar.Server.DTOs;
using Ar.Server.Exceptions;
using AutoMapper;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Ar.Server.ServerExtensionHelpers;
using Ar.Server.Validator.Core;

namespace Ar.Server.Accounting
{
    public class QuoteService : IService<QuoteDto>
    {

        private readonly ILogger<QuoteService> _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidator<QuoteDto> _validator;

        public QuoteService(ILogger<QuoteService> logger, IMapper mapper, IUnitOfWork unitOfWork, IValidator<QuoteDto> validator)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        IEnumerable<QuoteDto> IService<QuoteDto>.GetAll()
        {
            try
            {
                _logger.LogInformation("Start: GetAllQuotes");
                var quotes = _unitOfWork.QuoteRepository.GetAll();
                var quotesDto = _mapper.Map<IEnumerable<QuoteDto>>(quotes);
                _logger.LogInformation("End: GetAllQuotes");
                return quotesDto;
            }

            catch (InvalidOperationException e)
            {
                _logger.LogError($"#Error: QuoteService.GetAllQuotes{e.Message}");
                return null;
            }

            catch (Exception e)
            {
                _logger.LogError($"#Error: Error en GetAllQuotes: {e.Message}");
                throw new Exception($"{e.Message}");
            }
        }

        QuoteDto IService<QuoteDto>.Get(int id)
        {
            try
            {
                _logger.LogInformation($"Start QuoteService.GetQuote( {id.ToString()} )");
                var quote = _unitOfWork.QuoteRepository.Get(id);
                var quoteDto = _mapper.Map<QuoteDto>(quote);
                _logger.LogInformation($"End QuoteService.GetQuote( {id.ToString()} )");
                return quoteDto;
            }
            catch (InvalidOperationException e)
            {
                _logger.LogError($"#Error: QuoteService.GetQuote( {id.ToString()} ): {e.Message}");
                return null;
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error: QuoteService.GetQuote( {id.ToString()} ): {e.Message}");
                throw new ArgumentException(e.Message);
            }
        }

        bool IService<QuoteDto>.Post(QuoteDto quoteDto)
        {
            try
            {
                _logger.LogInformation($"Start: QuoteService.PostQuote()");
                this.ValidateDto(quoteDto, _validator, RuleSets.Create);
                var quote = _mapper.Map<Quote>(quoteDto);
                _unitOfWork.QuoteRepository.Add(quote);
                _unitOfWork.Commit();
                _logger.LogInformation($"End: QuoteService.PostQuote()");
                return true;
            }
            catch (DtoValidationException)
            {
                return false;
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error: QuoteService.PostQuote(): {e.Message}");
                throw new Exception(e.Message);
            }
        }

        void IService<QuoteDto>.Update(int id, QuoteDto quoteDto)
        {
            try
            {
                _logger.LogInformation($"Start: QuoteService.PutQuote");
                this.ValidateDto(quoteDto, _validator);
                var quote = _mapper.Map<Quote>(quoteDto);
                quote.Id = id;
                _unitOfWork.QuoteRepository.Update(quote);
                _unitOfWork.Commit();
                _logger.LogInformation($"End: QuoteService.PutQuote");
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error: QuoteService.PutQuote");
                throw new Exception(e.Message);
            }
        }

        void IService<QuoteDto>.Delete(int id)
        {
            try
            {
                _logger.LogInformation($"Start: QuoteService.DeleteQuote({id.ToString()})");
                _unitOfWork.QuoteRepository.Remove(id);
                _unitOfWork.Commit();
                _logger.LogInformation($"End: QuoteService.DeleteQuote({id.ToString()})");
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error QuoteService.DeleteQuote({id.ToString()}: {e.Message}");
                throw new Exception(e.Message);
            }
        }
    }
}
