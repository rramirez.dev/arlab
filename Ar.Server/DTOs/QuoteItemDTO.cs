﻿using Ar.Shared.Core;

namespace Ar.Server.DTOs
{
    public class QuoteItemDto: EntityBaseDto
    {
        public decimal Price { get; set; }
        public int QuoteId { get; set; }
        public int AnalysisId { get; set; }
    }
}