﻿using Ar.Shared.Core;
using System.Collections.Generic;

namespace Ar.Server.DTOs
{
    public class QuoteDto: EntityBaseDto
    {
        public decimal TotalPrice { get; set; }
        public ICollection<QuoteItemDto> QuoteItems { get; set; }
    }
}
