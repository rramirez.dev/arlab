﻿using Ar.Domain.Models.Enterprise;
using Ar.Server.DTOs;
using AutoMapper;

namespace Ar.Server.Mapper
{
    public class QuoteProfile: Profile
    {
        public QuoteProfile()
        {
            CreateMap<Quote, QuoteDto>()
                .ReverseMap();

            CreateMap<QuoteItem, QuoteItemDto>()
                .ReverseMap();
        }
    }
}
