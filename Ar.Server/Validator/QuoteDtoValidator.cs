﻿using Ar.Server.DTOs;
using Ar.Server.Validator.Core;
using FluentValidation;

namespace Ar.Server.Validator
{
    public class QuoteDtoResponseValidator: AbstractValidator<QuoteDto>
    {
        public QuoteDtoResponseValidator()
        {
            RuleSet(RuleSets.Create, () => {
                RuleFor(QuoteDto => QuoteDto.Id).Equal(default(int));
                RuleFor(QuoteDto => QuoteDto.TotalPrice).GreaterThan(0);
            });

            RuleSet(RuleSets.Update, () => {
                RuleFor(QuoteDto => QuoteDto.Id).NotNull();
            });
        }
    }
}
