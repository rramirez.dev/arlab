﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Server.Validator.Core
{
    public class RuleSets
    {
        public static string Create { get; } = "Create";
        public static string Read { get; } = "Read";
        public static string Update { get; } = "Update";
        public static string Delete { get; } = "Delete";
    }
}
