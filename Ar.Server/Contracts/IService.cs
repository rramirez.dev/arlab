﻿using Ar.Shared.Core;
using System.Collections.Generic;

namespace Ar.Server.Contracts
{
    public interface IService<T> where T : EntityBaseDto
    {
        IEnumerable<T> GetAll();

        T Get(int id);

        bool Post(T entity);

        void Update(int id, T entity);

        void Delete(int id);
    }
}
