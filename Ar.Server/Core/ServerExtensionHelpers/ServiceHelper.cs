﻿using Ar.Server.Contracts;
using Ar.Server.DTOs;
using Ar.Server.Exceptions;
using Ar.Shared.Core;
using FluentValidation;
using FluentValidation.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Server.ServerExtensionHelpers
{
    public static class ServiceHelper
    {
        public static void ValidateDto<T>(this IService<T> service, T entityBaseDto, IValidator<T> validator, string ruleSet = null) where T : EntityBaseDto
        {
            var result = validator.Validate(entityBaseDto, default(IValidatorSelector), ruleSet );
            if (!result.IsValid)
            {
                throw new DtoValidationException(result.Errors);
            }
        }


        public static void ValidateDto<T>(this IService<T> service, IEnumerable<T> entityBaseDtos, IValidator<T> validator) where T : EntityBaseDto
        {
            foreach (var entityBaseDto in entityBaseDtos)
            {
                var result = validator.Validate(entityBaseDto);
                if (!result.IsValid)
                {
                    throw new DtoValidationException(result.Errors);
                }
            }
        }
    }
}
