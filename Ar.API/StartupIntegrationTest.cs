﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Ar.Domain.Models;
using Ar.Server.Accounting;
using Ar.Server.Contracts;
using Ar.DAL.Repos.Contracts;
using Ar.DAL.Repos;
using Ar.Domain.Models.Enterprise;
using AutoMapper;
using Ar.Server.Mapper;
using Ar.Server.DTOs;
using FluentValidation;
using Ar.Server.Validator;
using FluentValidation.AspNetCore;

namespace Ar.API
{
    public class StartupIntegrationTest
    {
        public StartupIntegrationTest(IConfiguration configuration)
        {
            Configuration = configuration;
            ConnectionString = Configuration.GetConnectionString("ArLabTestConnectiongString");
        }

        public IConfiguration Configuration { get; }
        public static string ConnectionString { get; private set; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(QuoteProfile));
            services.AddMvc()
                .AddFluentValidation();
            services.AddDbContext<ArLabContext>(context => context.UseSqlServer(Configuration.GetConnectionString("ArLabTestConnectiongString")));
            services.AddScoped<IService<QuoteDto>, QuoteService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRepository<Quote>, QuoteRepository>();
            services.AddTransient<IValidator<QuoteDto>, QuoteDtoResponseValidator>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            //app.UseMvc(routes => 
            //    routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}"));
        }
    }
}
