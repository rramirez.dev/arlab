﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Ar.Domain.Models;
using Ar.Server.Accounting;
using Ar.Server.Contracts;
using Ar.DAL.Repos.Contracts;
using Ar.DAL.Repos;
using Ar.Domain.Models.Enterprise;
using AutoMapper;
using Ar.Server.Mapper;
using Ar.Server.DTOs;
using FluentValidation;
using FluentValidation.AspNetCore;
using Ar.Server.Validator;

namespace Ar.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(QuoteProfile));
            services.AddMvc()
                .AddFluentValidation();
            services.AddDbContext<ArLabContext>(context => context.UseSqlServer(Configuration.GetConnectionString("ArLabConnection")));
            services.AddScoped<IService<QuoteDto>, QuoteService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRepository<Quote>, QuoteRepository>();
            services.AddTransient<IValidator<QuoteDto>, QuoteDtoResponseValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc(routes => 
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}"));
        }
    }
}
