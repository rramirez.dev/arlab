﻿using System;
using Ar.Server.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ar.Server.DTOs;

namespace Ar.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {
        private readonly IService<QuoteDto> _quoteService;
        private readonly ILogger<QuotesController> _logger;

        public QuotesController(IService<QuoteDto> quoteService, ILogger<QuotesController> logger)
        {
            _quoteService = quoteService;
            _logger = logger;
        }

        [HttpGet]
        [HttpGet("all")]
        public IActionResult Get()
        {
            try
            {
                _logger.LogInformation("Start: QuotesController.Get()");
                var quotes = _quoteService.GetAll();
                if (quotes != null)
                    return Ok(quotes);
                else
                    return NotFound();
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error Get api/quote:  {e.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
            finally
            {
                _logger.LogInformation("End: QuotesController.Get()");
            }
        }

        // GET api/quote/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                _logger.LogInformation($"Start QuotesController.Get({id.ToString()})");
                var quote = _quoteService.Get(id);
                if (quote != null)
                    return Ok(quote);
                else
                    return NotFound();
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error QuotesController.Get({id.ToString()}) + {e.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
            finally
            {
                _logger.LogInformation($"End: QuotesController.Get({id.ToString()})");
            }
        }

        // POST api/quote
        [HttpPost]
        public IActionResult Post([FromBody] QuoteDto quote)
        {
            try
            {
                _logger.LogInformation($"Start: QuotesController.Post()");

                var flag = _quoteService.Post(quote);
                if (!flag) {
                    return BadRequest();
                }
                else {
                    return Created("api/quote", quote);
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error: QuotesController.Post(): {e.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
            finally
            {
                _logger.LogInformation("End: QuotesController.Post({})");
            }
        }

        // PUT api/quote/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] QuoteDto dto)
        {
            _logger.LogInformation($"Start: QuotesController.Put({dto.ToString()})");
            try
            {
                if (ModelState.IsValid)
                {
                    _quoteService.Update(id, dto);
                    _logger.LogInformation($"End: QuotesController.Put{dto.ToString()}");
                    return Ok();
                }
                else
                    return BadRequest();
                
            }
            catch(Exception e)
            {
                _logger.LogError($"#Error QuotesController.Put: {e.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        // DELETE api/quote/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _logger.LogInformation($"Start: QuotesController.Delete{id.ToString()}");
            try
            {
                if (ModelState.IsValid){
                    _quoteService.Delete(id);
                    return Ok();
                }
                else
                    return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError($"#Error QuotesController.Delete{id.ToString()}: {e.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
            finally
            {
                _logger.LogInformation($"End: QuotesController.Delete({id.ToString()}");
            }
        }
    }
}
