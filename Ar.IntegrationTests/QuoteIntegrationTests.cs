using Ar.Domain.Models.Enterprise;
using Ar.IntegrationTests.Core;
using Ar.Server.Contracts;
using Ar.Server.DTOs;
using Ar.Server.Exceptions;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Ar.IntegrationTests
{
    public class QuoteIntegrationIntegrationTests : BaseIntegrationTest
    {
        readonly IService<QuoteDto> _quoteService;
        public QuoteIntegrationIntegrationTests(FixtureIntegrationTest fixture) : base (fixture)
        {
            _quoteService = Services.GetService(typeof(IService<QuoteDto>)) as IService<QuoteDto>;
        }

        #region Positive tests

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verify that retrieves all quotes in database")]
        public void GivenContextWithEntities_ShoulRetrieveAllOfThem()
        {
            //Arrange
            Context.SetupDatabaseForTesting();
            var q1 = new Quote() { Id = default };
            var q2 = new Quote() { Id = default };
            var list = new List<Quote>() { q1, q2 };
            Context.SeedDatabaseWith(list);

            //Act
            var result =  _quoteService.GetAll();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
        }

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verify that retrieves a QuoteDto by Id")]
        public void GivenContextWithEntities_ShouldRetrieveSingleMatchingId()
        {
            //Arrange
            Context.SetupDatabaseForTesting();
            Context.SeedDatabaseWithDummy<Quote>();
            Quote expected = DatabaseHelper.EntitiesSeeded.SingleOrDefault() as Quote;

            //Act
            QuoteDto actual = _quoteService.Get(expected.Id);

            //Assert
            Assert.Equal(expected.Id, actual.Id);
            Assert.NotNull(actual);
        }

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verify that saves a Quote in database")]
        public void GivenValidQuoteDto_ShouldStoreInDatabaseSingleQuote()
        {
            //Arrange
            Context.SetupDatabaseForTesting();
            int InitialQuoteCount = Context.Quotes.Count();
            var QuoteDto = DataFactory.CreateInstance<QuoteDto>()
                .WithPropertyValue("Id", default(int));

            //Act
            _quoteService.Post(QuoteDto as QuoteDto);
            
            //Assert
            int FinalQuoteCount = Context.Quotes.Count();
            Quote quoteStoredInDatabase = Context.Quotes.SingleOrDefault();
            Assert.NotEqual(InitialQuoteCount, FinalQuoteCount);
            Assert.NotNull(quoteStoredInDatabase);
        }

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verify that given a valid Dto, updates quote in database")]
        public void GivenValidDto_ShouldUpdateQuote()
        {
            //Arrange
            Context.SetupDatabaseForTesting();
            var quote = new Quote{ TotalPrice = 100 };
            Context.SeedDatabaseWith(quote);
            var quoteDto = new QuoteDto { Id = quote.Id, TotalPrice = 200 };

            //Act
            _quoteService.Update(DatabaseHelper.EntitySeeded.Id, quoteDto);

            //Assert
            Assert.Equal(quoteDto.TotalPrice, quote.TotalPrice);
            Assert.NotNull(quote);
        }

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verify that given a valid quote id, delete the quote from database")]
        public void GivenValidQuoteId_ShouldDeleteQuoteFromDatabase()
        {
            //Arrange
            Context.SetupDatabaseForTesting();
            var quote = new Quote { TotalPrice = 100 };
            Context.SeedDatabaseWith(quote);
            int QuoteCountBeforeDelete = Context.Quotes.Count();

            //Act
            _quoteService.Delete(quote.Id);
            int QuoteCountAfterDelete = Context.Quotes.Count();

            //Assert
            Assert.NotEqual(0, QuoteCountBeforeDelete);
            Assert.Equal(0, QuoteCountAfterDelete);
        }

        #endregion

        #region negative tests

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Theory(DisplayName = "Verify that retrieves throws null if Id is not found")]
        [InlineData(-1)]
        public void GivenInvalidId_ShouldThrowError(int wrongId)
        {
            //Arrange
            Context.SetupDatabaseForTesting();

            //Act
            var result = _quoteService.Get(wrongId);

            //Assert
            Assert.Null(result);
        }

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verifys that retrieves an empty list if there are no data in context")]
        public void GivenEmptyCollection_ShouldReturnAnEmptyListOfQuotes()
        {
            //Arrange
            Context.SetupDatabaseForTesting();

            //Act
            var result = _quoteService.GetAll();

            //Assert
            Assert.Empty(result);
            Assert.NotNull(result);
        }

        [Trait(TestConstants.Category, TestConstants.Integration)]
        [Fact(DisplayName = "Verifys that posting a invalid dto, validation throws exception")]
        public void GivenInvalidQuoteDto_ShouldThrowValidationException()
        {
            //Arrange
            Context.SetupDatabaseForTesting();

            var invalidModel = DataFactory.CreateInstance<QuoteDto>()
                .WithPropertyValue("Id", -1)
                .WithPropertyValue("TotalPrice", (decimal) -1);

            //Act
            var flag = _quoteService.Post(invalidModel as QuoteDto);

            //Assert

            Assert.False(flag);
        }

        #endregion
    }
}
