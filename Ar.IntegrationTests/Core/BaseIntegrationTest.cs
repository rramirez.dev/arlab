﻿using Ar.Domain.Models;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Xunit;

namespace Ar.IntegrationTests.Core
{
    [Collection("Integration Test Collection")]
    public class BaseIntegrationTest
    {
        protected FixtureIntegrationTest Fixture { get; private set; }
        protected TestServer Server { get; private set; }
        protected HttpClient Client { get; private set; }
        protected IServiceProvider Services { get; private set; }
        protected ArLabContext Context { get; private set; }

        public BaseIntegrationTest(FixtureIntegrationTest fixture)
        {
            Fixture = fixture;
            Server = Fixture.Server;
            Client = Server.CreateClient();
            Services = Server.Host.Services;
            Context = Services.GetService(typeof(ArLabContext)) as ArLabContext;
        }
    }
}
