﻿using Ar.API;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Net.Http;
using Xunit;

namespace Ar.IntegrationTests.Core
{
    public class FixtureIntegrationTest : IDisposable
    {
        private string _env = "Testing";
        public TestServer Server { get; set; }
        public HttpClient HttpClient { get; set; }
        public IServiceProvider Services { get; set; }
        public FixtureIntegrationTest()
        {
            var configBuilder = GetConfigurationBuilder();
            Server = GetTestServer(configBuilder);
            HttpClient = Server.CreateClient();
            Services = Server.Host.Services;
        }
        private IConfigurationBuilder GetConfigurationBuilder()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_env}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder;
        }
        private TestServer GetTestServer(IConfigurationBuilder config)
        {
            var server = new TestServer(WebHost.CreateDefaultBuilder()
                .UseEnvironment(_env)
                .UseConfiguration(config.Build())
                .UseStartup<StartupIntegrationTest>());

            return server;
        }
        public void Dispose()
        {
            Server.Dispose();
            HttpClient.Dispose();
        }
    }

    [CollectionDefinition("Integration Test Collection")]
    public class IntegrationTestCollection : ICollectionFixture<FixtureIntegrationTest>
    {

    }
}
