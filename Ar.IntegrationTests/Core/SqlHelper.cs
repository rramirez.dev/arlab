﻿using Ar.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Ar.IntegrationTests.Core
{
    public static class SqlHelper
    {
        public static string DisableTablesConstraintCommand = "EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL' ";
        public static string DeleteDatabaseDataCommand = "EXEC sp_MSForEachTable 'SET QUOTED_IDENTIFIER ON; DELETE FROM ?'";
        public static string EnableTablesConstraintCommand = "EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'";
    }

    public static class DatabaseHelper
    {
        public static IEnumerable<EntityBase> EntitiesSeeded;
        public static EntityBase EntitySeeded;


        public static void SetupDatabaseForTesting(this ArLabContext arlabContext)
        {
            EntitiesSeeded = null;
            arlabContext.Database.EnsureCreated();
            arlabContext.Database.ExecuteSqlCommand(SqlHelper.DisableTablesConstraintCommand);
            arlabContext.Database.ExecuteSqlCommand(SqlHelper.DeleteDatabaseDataCommand);
            arlabContext.Database.ExecuteSqlCommand(SqlHelper.EnableTablesConstraintCommand);
        }

        public static void SeedDatabaseWith<T>(this ArLabContext arLabContext, IEnumerable<T> entities) where T : EntityBase
        {
            List<T> entityList = new List<T>();

            foreach (var entity in entities)
            {
                entityList.Add(entity as T);
            }

            arLabContext.Set<T>().AddRange(entityList);
            arLabContext.SaveChanges();
            EntitiesSeeded = entityList;
        }

        public static void SeedDatabaseWith<T>(this ArLabContext arLabContext, T entity) where T : EntityBase
        {
            arLabContext.Set<T>().Add(entity);
            arLabContext.SaveChanges();
            EntitySeeded = entity;
        }

        public static void SeedDatabaseWithDummy<T>(this ArLabContext arLabContext, int entityCount = 1) where T : EntityBase
        {
            var entityList = new List<T>();
            if (entityCount > 0)
            {
                for (int i = 0; i < entityCount; i++)
                {
                    var dummyEntity = DataFactory.CreateInstance<T>()
                        .WithPropertyValue("Id", default(int));

                    entityList.Add((T) dummyEntity);
                }

                arLabContext.Set<T>().AddRange(entityList);
                arLabContext.SaveChanges();
                EntitiesSeeded = entityList;
            }

            else
            {
                EntitiesSeeded = default;
                throw new System.Exception("Entity cannot be less than 0");
            }
        }
    }
}
