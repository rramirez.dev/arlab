﻿using Ar.Domain.Models;
using Ar.Domain.Models.Enterprise;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Ar.IntegrationTests.Core
{
    public static class DataFactory
    {
        private static readonly Random rnd = new Random();

        /// <summary>
        /// Attempts to create a valid instance of a given class T. In case it cannot resolve a 
        /// dummy data for a given property, it set it to the default value of object (null)
        /// </summary>
        /// <typeparam name="T">The type of the class to be instantiated</typeparam>
        /// <returns>Instance of the supplied class, filled with dummy data</returns>
        public static object CreateInstance<T>()
        {
            var obj = Activator.CreateInstance(typeof(T));

            try
            {
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    TypeCode typeCode = Type.GetTypeCode(prop.PropertyType);
                    DataManipulator.FillObjectPropertiesWithData<T>(typeCode, prop, obj);
                }

                return (T)Convert.ChangeType(obj, typeof(T));
            }

            catch (Exception e)
            {
                string message = e.Message;
                return obj;
            }
        }

        /// <summary>
        /// Fill the given object property with specific data supplied. Can be chained up to create a specific instance of an object
        /// </summary>
        /// <typeparam name="T">The type of the class to be instantiated</typeparam>
        /// <param name="obj">The object whose properties will be changed by this method</param>
        /// <param name="propertyName">Property name of the property that would be updated with a new value</param>
        /// <param name="propertyValue">Value that would be set</param>
        /// <returns></returns>
        public static T WithPropertyValue<T>(this T obj, string propertyName, object propertyValue)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(propertyName))
                {
                    throw new ArgumentException("propertyName cannot be null, empty, or white spaces");
                }

                PropertyInfo prop = obj.GetType().GetProperty(propertyName);
                prop.SetValue(obj, propertyValue);
                return obj;
            }
            catch (Exception)
            {
                return obj;
            }
        }

        public static class DataManipulator
        {
            public static T FillObjectPropertiesWithData<T>(TypeCode typeCode, PropertyInfo prop, object obj)
            {

                switch (typeCode)
                {
                    case TypeCode.String:
                        prop.SetValue(obj, GetValidString());
                        break;
                    case TypeCode.Int32:
                        prop.SetValue(obj, GetValidNumber());
                        break;
                    case TypeCode.Decimal:
                        prop.SetValue(obj, GetValidDecimal());
                        break;
                    case TypeCode.DateTime:
                        prop.SetValue(obj, GetValidDateTime());
                        break;
                    case TypeCode.Boolean:
                        prop.SetValue(obj, true);
                        break;
                    case TypeCode.Object:
                        SetNestedProperties(prop, obj);
                        break;
                }

                return Activator.CreateInstance<T>();
            }

            private static void SetNestedProperties(PropertyInfo prop, object obj)
            {
                //if (prop is IEnumerable<QuoteItem>)
                if (prop.PropertyType as Type is IEnumerable<EntityBase>)
                {
                    foreach (var entity in prop as IEnumerable<QuoteItem>)
                    {
                        var en = CreateInstance<EntityBase>();
                    }
                }

                if (obj is EntityBase)
                {
                    foreach (PropertyInfo innerProp in obj.GetType().GetProperties())
                    {
                        TypeCode typeCode = Type.GetTypeCode(innerProp.PropertyType);
                        var item = DataManipulator.FillObjectPropertiesWithData<EntityBase>(typeCode, innerProp, obj);
                    }
                }

                else
                {
                    prop.SetValue(obj, true);
                }
            }

            public static DateTime GetValidDateTime()
            {
                var start = new DateTime(1950, 1, 1);
                int range = (DateTime.Today - start).Days;
                return start.AddDays(rnd.Next(range));
            }
            public static decimal GetValidDecimal()
            {
                return (decimal)rnd.NextDouble();
            }
            public static int GetValidNumber()
            {
                return rnd.Next(1, 100);
            }
            public static string GetValidString()
            {
                return $"{Guid.NewGuid()}";
            }
        }
    }
}
