﻿using Ar.DAL.Repos;
using Ar.DAL.Repos.Contracts;
using Ar.Domain.Models;
using Ar.Domain.Models.Enterprise;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.DAL.Repos
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ArLabContext _dbContext;
        private IRepository<Quote> _quoteRepository;

        public IRepository<Quote> QuoteRepository
        {
            get
            {
                return _quoteRepository ?? new QuoteRepository(_dbContext);
            }
        }

        public UnitOfWork(ArLabContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
