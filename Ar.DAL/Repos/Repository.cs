﻿using Ar.Domain.Models;
using Ar.DAL.Repos.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ar.DAL.Repos
{
    public class Repository<T> : IRepository<T> where T : EntityBase
    {
        private readonly ArLabContext _dbContext;

        public Repository(ArLabContext context)
        {
            _dbContext = context;
        }

        public virtual void Add(T entity)
        {
            _dbContext.Add(entity);
        }

        public virtual void Remove(int id)
        {
            var entity = _dbContext.Set<T>().Where(c => c.Id == id).First();
            _dbContext.Remove(entity);
        }

        public virtual void Attach(T entity)
        {
            if (entity != null)
                _dbContext.Set<T>().Attach(entity);
            else
                throw new KeyNotFoundException();
        }

        public virtual T Get(int id)
        {
            return _dbContext.Set<T>().Where(c => c.Id == id).First();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public virtual void Update(T entity)
        {
            var ent = _dbContext.Set<T>().First(_ => _.Id == entity.Id);
            _dbContext.Entry(ent).CurrentValues.SetValues(entity);
        }

        public bool Exists(int id)
        {
            return _dbContext.Set<T>().Where(c => c.Id == id).Count() > 0 ? true : false;
        }
    }
}
