﻿using Ar.Domain.Models;
using Ar.Domain.Models.Enterprise;
using Ar.DAL.Repos.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ar.DAL.Repos
{
    public class QuoteRepository : Repository<Quote>
    {
        private readonly ArLabContext _arLabContext;
        public QuoteRepository(ArLabContext context): base(context)
        {
            _arLabContext = context;
        }

        public override void Remove(int id)
        {
            var entity = _arLabContext.Quotes
                .Where(c => c.Id == id)
                    //.Include(c => c.QuoteItems)
                .First();

            _arLabContext.Remove(entity);
        }

        public override IEnumerable<Quote> GetAll()
        {
            var entities = _arLabContext.Quotes
                //.Include(c => c.QuoteItems)
                .ToList();

            return entities;
        }

        public override Quote Get(int id)
        {
            var entity = _arLabContext.Quotes
                .Where(c => c.Id == id)
                    //.Include(c => c.QuoteItems)
                .First();

            return entity;
        }
    }
}
