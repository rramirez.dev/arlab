﻿using Ar.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ar.DAL.Repos.Contracts
{
    public interface IRepository<T> where T: EntityBase
    {
        void Remove(int id);
        void Add(T entity);
        void Update(T entity);
        void Attach(T entity);
        IEnumerable<T> GetAll();
        T Get(int id);
        bool Exists(int id);
    }
}
