﻿using Ar.Domain.Models.Enterprise;
using Ar.DAL.Repos.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.DAL.Repos.Contracts
{
    public interface IUnitOfWork
    {
        IRepository<Quote> QuoteRepository { get; }
        void Commit();
        void Dispose();
    }
}
