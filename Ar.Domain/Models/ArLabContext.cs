﻿using Ar.Domain.Models.Configs;
using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Handy;
using Ar.Domain.Models.Identity;
using Ar.Domain.Models.Laboratory;
using Ar.Domain.Models.Share;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models
{
    public class ArLabContext: DbContext, IDatabaseContext
    {
        public ArLabContext(DbContextOptions<ArLabContext> options): base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<AccountingRecord> Accountings { get; set; }
        public DbSet<AccountingRecordType> AccountingTypes { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<QuoteItem> QuoteItems { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<RequestItem> RequestItems { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AnalysisParameter> AnalysisParameters { get; set; }
        public DbSet<DeliverStatus> DeliverStatuss { get; set; }
        public DbSet<Analysis> Analysis { get; set; }
        public DbSet<AnalysisTypeParameterRelationship> AnalysisTypeParameterRelationship { get; set; }
        public DbSet<AnalysisType> TaskTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Seed();

            //database model configuration
            modelBuilder.ApplyConfiguration(new AccountingRecordConfiguration());
            modelBuilder.ApplyConfiguration(new AccountingRecordTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AnalysisConfiguration());
            modelBuilder.ApplyConfiguration(new AnalysisParameterConfiguration());
            modelBuilder.ApplyConfiguration(new AnalysisTypeParameterRelationshipConfiguration());
            modelBuilder.ApplyConfiguration(new AnalysisTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DeliverStatusConfiguration());
            modelBuilder.ApplyConfiguration(new OrganizationConfiguration());
            modelBuilder.ApplyConfiguration(new QuoteConfiguration());
            modelBuilder.ApplyConfiguration(new QuoteItemConfiguration());
            modelBuilder.ApplyConfiguration(new RequestConfiguration());
            modelBuilder.ApplyConfiguration(new RequestItemConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new OrganizationConfiguration());

        }

    }
}
