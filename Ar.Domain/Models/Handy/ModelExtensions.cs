﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Laboratory;
using Ar.Domain.Models.Share;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ar.Domain.Models.Handy
{
    internal static class ModelExtensions
    {
        public static class TestRepository
        {
            internal static IEnumerable<AnalysisType> GetAnalysisType()
            {
                var analysisType = new AnalysisType()
                {
                    Id = 1,
                    AnalysisTypeDescription = "Quimica",
                };

                var analysisTypes = new List<AnalysisType>();
                analysisTypes.Add(analysisType);

                return analysisTypes;
            }
            internal static IEnumerable<AnalysisParameter> GetAnalysisParameter()
            {
                var analysisParameter = new AnalysisParameter()
                {
                    Id = 1,
                    AnalysisParameterDescription = "THC",
                };

                var analysisParameters = new List<AnalysisParameter>()
                {
                    analysisParameter
                };

                return analysisParameters;
            }
            internal static IEnumerable<AnalysisTypeParameterRelationship> GetAnalysisTypeParameterRelationship()
            {


                var analysisTypeParameterRelationship = new AnalysisTypeParameterRelationship()
                {
                    Id = 1,
                    AnalysisParameterId = 1,
                    AnalysisTypeId = 1
                };

                var analysisTypeParameterRelationshipList = new List<AnalysisTypeParameterRelationship>
                {
                    analysisTypeParameterRelationship
                };

                return analysisTypeParameterRelationshipList;

            }
            internal static IEnumerable<Analysis> GetAnalyses()
            {
                Analysis analysisA = new Analysis()
                {
                    Id = 1,
                    AnalysisTypeId = null,
                    AnalysisDescription = "Analisis de cannabinoides",
                };

                Analysis analysisB = new Analysis()
                {
                    Id = 2,
                    AnalysisTypeId = 1,
                    AnalysisDescription = "Analisis de terpenos",
                };

                var analyses = new List<Analysis>
                {
                    analysisA,
                    analysisB
                };

                return analyses;
            }
            //internal static IEnumerable<QuoteItem> GetQuoteItems()
            //{
            //    //QuoteItem quoteItemA = new QuoteItem(1)
            //    //{
            //    //    Id = 1,
            //    //    Price = 500,
            //    //    AnalysisId = 1
            //    //};
            //    //QuoteItem quoteItemB = new QuoteItem(2)
            //    //{
            //    //    Id = 2,
            //    //    Price = 250,
            //    //    AnalysisId = 2
            //    //};

            //    var quoteItems = new List<QuoteItem>
            //    {
            //        quoteItemA,
            //        quoteItemB
            //    };

            //    return quoteItems;
            //}
            internal static IEnumerable<Quote> GetQuotes()
            {
                Quote quoteA = new Quote()
                {
                    Id = 1,
                    TotalPrice = 1000
                };

                var quotes = new List<Quote>(){
                    quoteA
                };

                return quotes;
            }

        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Quote>().HasData(TestRepository.GetQuotes());
            //modelBuilder.Entity<QuoteItem>().HasData(TestRepository.GetQuoteItems());
            modelBuilder.Entity<Analysis>().HasData(TestRepository.GetAnalyses());
            modelBuilder.Entity<AnalysisTypeParameterRelationship>().HasData(TestRepository.GetAnalysisTypeParameterRelationship());
            modelBuilder.Entity<AnalysisParameter>().HasData(TestRepository.GetAnalysisParameter());
            modelBuilder.Entity<AnalysisType>().HasData(TestRepository.GetAnalysisType());
        }

    }
}
