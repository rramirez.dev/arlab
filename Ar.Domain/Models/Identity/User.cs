﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Identity
{
    public class User : EntityBase
    {
        public string Pass { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsBoss { get; set; }
        public int OrganizationId { get; set; }
    }
}
