﻿using Ar.Domain.Models.Enterprise;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Share
{
    public class Analysis : EntityBase
    {
        public string AnalysisDescription { get; set; }
        public int? AnalysisTypeId { get; set; }
        public QuoteItem QuoteItem { get; set; }
    }
}
