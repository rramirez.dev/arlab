﻿using Ar.Domain.Models.Laboratory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Share
{
    public class AnalysisTypeParameterRelationship : EntityBase
    {
        public int AnalysisTypeId { get; set; }
        public AnalysisType AnalysisType { get; set; }
        public int AnalysisParameterId { get; set; }
        public AnalysisParameter AnalysisParameter { get; set; }

    }
}
