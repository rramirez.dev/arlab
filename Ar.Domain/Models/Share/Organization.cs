﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Share
{
    public class Organization : EntityBase
    {
        public string OrganizationName { get; set; }
    }
}
