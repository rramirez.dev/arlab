﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Share
{
    public class DeliverStatus : EntityBase
    {
        public string DeliverStatusDescription { get; set; }
    }
}
