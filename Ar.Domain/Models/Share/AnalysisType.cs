﻿using Ar.Domain.Models.Laboratory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Share
{
    public class AnalysisType : EntityBase
    {
        public string AnalysisTypeDescription { get; set; }
        public ICollection<AnalysisTypeParameterRelationship> analysisTypeParameterRelationships { get; set; }
    }
}
