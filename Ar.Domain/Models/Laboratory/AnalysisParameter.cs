﻿using Ar.Domain.Models.Share;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Laboratory
{
    public class AnalysisParameter : EntityBase
    {
        public string AnalysisParameterDescription { get; set; }
        public ICollection<AnalysisTypeParameterRelationship> analysisTypeParameterRelationships { get; set; }
    }
}
