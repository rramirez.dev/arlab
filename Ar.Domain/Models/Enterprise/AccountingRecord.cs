﻿using Ar.Domain.Models.Share;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Enterprise
{
    public class AccountingRecord : EntityBase
    {
        public int AnalysisId { get; set; }
        public int DeliverStatusId { get; set; }
        public int UserId { get; set; }
        public int OrganizationId { get; set; }
        public int AccountingRecordTypeId { get; set; }
    }
}
