﻿using Ar.Domain.Models.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Enterprise
{
    public class Quote : EntityBase
    {
        public Quote()
        {
            DateIssued = DateTime.Now;
            DateExpire = DateTime.Now.AddDays(30);
        }

        public decimal TotalPrice { get; set; }
        public ICollection<QuoteItem> QuoteItems { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime DateExpire { get; set; }
        public int OrganizationId { get; set; }
        //public User User { get; set; }
    }
}
