﻿using Ar.Domain.Models.Share;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Enterprise
{
    public class QuoteItem : EntityBase
    {
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int AnalysisId { get; set; }
        public Analysis Analysis { get; set;  }
    }
}
