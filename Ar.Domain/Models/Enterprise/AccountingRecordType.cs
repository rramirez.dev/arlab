﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Enterprise
{
    public class AccountingRecordType : EntityBase
    {
        public int AccountingRecordTypeDescription { get; set; }
    }
}
