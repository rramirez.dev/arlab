﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Enterprise
{
    public class RequestItem : EntityBase
    {
        public int RequestId { get; set; }
        public int DeliverStatusId { get; set; }
        public int AnalysisId { get; set; }
    }
}
