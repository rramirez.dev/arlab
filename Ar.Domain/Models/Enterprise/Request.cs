﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Enterprise
{
    public class Request : EntityBase
    {
        public DateTime OriginDate { get; set; }
        public DateTime DeliverDate { get; set; }
        public int DeliverStatusId { get; set; }
        public int UserId { get; set; }
        public ICollection<RequestItem> RequestItems { get; set; }
    }
}
