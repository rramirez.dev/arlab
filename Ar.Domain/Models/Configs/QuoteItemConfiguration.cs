﻿using Ar.Domain.Models.Enterprise;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class QuoteItemConfiguration : IEntityTypeConfiguration<QuoteItem>
    {
        public void Configure(EntityTypeBuilder<QuoteItem> builder)
        {
            builder.ToTable(Constants.Tables.QuoteItem);

            builder.Property(pr => pr.Id).HasColumnName(Constants.QuoteItem.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.Price).HasColumnName(Constants.QuoteItem.Price);
            builder.Property(pr => pr.AnalysisId).HasColumnName(Constants.Analysis.Id);
            builder.Property(pr => pr.Description).HasColumnName(Constants.QuoteItem.Description);
        }
    }
}