﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Share;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class AnalysisTypeParameterRelationshipConfiguration : IEntityTypeConfiguration<AnalysisTypeParameterRelationship>
    {
        public void Configure(EntityTypeBuilder<AnalysisTypeParameterRelationship> builder)
        {
            builder.ToTable(Constants.Tables.AnalysisTypeParameterRelationship);

            builder.Property(pr => pr.Id).HasColumnName(Constants.AnalysisParameterTypeRelationship.Id).ValueGeneratedOnAdd();
            builder.HasKey(pr => new { pr.AnalysisParameterId, pr.AnalysisTypeId });

            builder.HasOne(pr => pr.AnalysisParameter)
                .WithMany(pr => pr.analysisTypeParameterRelationships)
                .HasForeignKey(pr => pr.AnalysisParameterId);
            builder.HasOne(pr => pr.AnalysisType)
                .WithMany(pr => pr.analysisTypeParameterRelationships)
                .HasForeignKey(pr => pr.AnalysisTypeId);

        }
    }
}