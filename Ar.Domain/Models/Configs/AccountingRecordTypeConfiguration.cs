﻿using Ar.Domain.Models.Enterprise;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Configs
{
    internal class AccountingRecordTypeConfiguration : IEntityTypeConfiguration<AccountingRecordType>
    {
        public void Configure(EntityTypeBuilder<AccountingRecordType> builder)
        {
            builder.ToTable(Constants.Tables.AccountingRecordType);

            builder.Property(pr => pr.Id).HasColumnName(Constants.AccountingRecordType.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.AccountingRecordTypeDescription).HasColumnName(Constants.AccountingRecordType.Description);
        }
    }
}
