﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Share;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class DeliverStatusConfiguration : IEntityTypeConfiguration<DeliverStatus>
    {
        public void Configure(EntityTypeBuilder<DeliverStatus> builder)
        {
            builder.ToTable(Constants.Tables.DeliverStatus);

            builder.Property(pr => pr.Id).HasColumnName(Constants.Shared.DeliverStatusId).ValueGeneratedOnAdd();
            builder.Property(pr => pr.DeliverStatusDescription).HasColumnName(Constants.Deliver.Description);
        }
    }
}