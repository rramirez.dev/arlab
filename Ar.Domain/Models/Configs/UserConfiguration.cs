﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Identity;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(Constants.Tables.User);

            builder.Property(pr => pr.Id).HasColumnName(Constants.Shared.UserId).ValueGeneratedOnAdd();
            builder.Property(pr => pr.FirstName).HasColumnName(Constants.User.FirstName);
            builder.Property(pr => pr.LastName).HasColumnName(Constants.User.LastName);
            builder.Property(pr => pr.OrganizationId).HasColumnName(Constants.Shared.OrganizationId);
            builder.Property(pr => pr.Pass).HasColumnName(Constants.User.Pass);
            builder.Property(pr => pr.IsBoss).HasColumnName(Constants.User.IsBoss);
            builder.Property(pr => pr.Email).HasColumnName(Constants.User.Email);
        }
    }
}