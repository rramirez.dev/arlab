﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Share;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Configs
{
    internal class AnalysisTypeConfiguration : IEntityTypeConfiguration<AnalysisType>
    {
        public void Configure(EntityTypeBuilder<AnalysisType> builder)
        {
            builder.ToTable(Constants.Tables.AnalysisType);

            builder.Property(pr => pr.Id).HasColumnName(Constants.AnalysisType.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.AnalysisTypeDescription).HasColumnName(Constants.AnalysisType.Description);
        }
    }
}
