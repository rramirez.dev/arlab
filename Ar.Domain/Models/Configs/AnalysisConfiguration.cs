﻿using Ar.Domain.Models.Share;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class AnalysisConfiguration : IEntityTypeConfiguration<Analysis>
    {
        public void Configure(EntityTypeBuilder<Analysis> builder)
        {
            builder.ToTable(Constants.Tables.Analysis);

            builder.Property(pr => pr.Id).HasColumnName(Constants.Analysis.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.AnalysisDescription).HasColumnName(Constants.Analysis.Description);
            builder.Property(pr => pr.AnalysisTypeId).HasColumnName(Constants.AnalysisType.Id);
        }

    }
}
