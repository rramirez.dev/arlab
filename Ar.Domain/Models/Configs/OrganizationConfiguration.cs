﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Share;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class OrganizationConfiguration : IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.ToTable(Constants.Tables.Organization);

            builder.Property(pr => pr.Id).HasColumnName(Constants.Shared.OrganizationId).ValueGeneratedOnAdd();
            builder.Property(pr => pr.OrganizationName).HasColumnName(Constants.Organization.Name);
        }
    }
}