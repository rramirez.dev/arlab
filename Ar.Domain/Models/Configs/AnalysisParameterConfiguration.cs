﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Laboratory;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class AnalysisParameterConfiguration : IEntityTypeConfiguration<AnalysisParameter>
    {
        public void Configure(EntityTypeBuilder<AnalysisParameter> builder)
        {
            builder.ToTable(Constants.Tables.AnalysisParameter);

            builder.Property(c => c.Id).HasColumnName(Constants.AnalysisParameter.Id).ValueGeneratedOnAdd();
            builder.Property(c => c.AnalysisParameterDescription).HasColumnName(Constants.AnalysisParameter.Description);
        }
    }
}