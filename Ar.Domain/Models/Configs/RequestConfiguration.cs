﻿using Ar.Domain.Models.Enterprise;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class RequestConfiguration : IEntityTypeConfiguration<Request>
    {
        public void Configure(EntityTypeBuilder<Request> builder)
        {
            builder.ToTable(Constants.Tables.Request);

            builder.Property(pr => pr.Id).HasColumnName(Constants.Request.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.OriginDate).HasColumnName(Constants.Request.OriginDate);
            builder.Property(pr => pr.DeliverDate).HasColumnName(Constants.Request.DeliverDate);
            builder.Property(pr => pr.DeliverStatusId).HasColumnName(Constants.Shared.DeliverStatusId);
            builder.Property(pr => pr.UserId).HasColumnName(Constants.Shared.UserId);
        }
    }
}