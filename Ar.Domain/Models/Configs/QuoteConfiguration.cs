﻿using Ar.Domain.Models.Enterprise;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ar.Domain.Models.Configs
{
    internal class QuoteConfiguration : IEntityTypeConfiguration<Quote>
    {
        public void Configure(EntityTypeBuilder<Quote> builder)
        {
            builder.ToTable(Constants.Tables.Quote);

            builder.Property(pr => pr.Id).HasColumnName(Constants.Quote.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.TotalPrice).HasColumnName(Constants.Quote.TotalPrice);
            builder.Property(pr => pr.OrganizationId).HasColumnName(Constants.Shared.OrganizationId);
        }
    }
}