﻿using Ar.Domain.Models.Enterprise;
using Ar.Shared.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;



namespace Ar.Domain.Models.Configs
{
    internal class RequestItemConfiguration : IEntityTypeConfiguration<RequestItem>
    {
        public void Configure(EntityTypeBuilder<RequestItem> builder)
        {
            builder.ToTable(Constants.Tables.RequestItem);

            builder.Property(pr => pr.Id).HasColumnName(Constants.RequestItem.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.RequestId).HasColumnName(Constants.Request.Id);
            builder.Property(pr => pr.DeliverStatusId).HasColumnName(Constants.Shared.DeliverStatusId);
            builder.Property(pr => pr.AnalysisId).HasColumnName(Constants.Analysis.Id);
        }
    }
}