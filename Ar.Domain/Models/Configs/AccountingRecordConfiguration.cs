﻿using Ar.Domain.Models.Enterprise;
using Ar.Domain.Models.Share;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ar.Shared.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ar.Domain.Models.Configs
{
    internal class AccountingRecordConfiguration: IEntityTypeConfiguration<AccountingRecord> 
    {
        public void Configure(EntityTypeBuilder<AccountingRecord> builder)
        {
            builder.ToTable(Constants.Tables.AccountingRecord);

            builder.Property(pr => pr.Id).HasColumnName(Constants.AccountingRecord.Id).ValueGeneratedOnAdd();
            builder.Property(pr => pr.AnalysisId).HasColumnName(Constants.Shared.AnalysisId);
            builder.Property(pr => pr.DeliverStatusId).HasColumnName(Constants.Shared.DeliverStatusId);
            builder.Property(pr => pr.UserId).HasColumnName(Constants.Shared.UserId);
            builder.Property(pr => pr.OrganizationId).HasColumnName(Constants.Shared.OrganizationId);
            builder.Property(pr => pr.AccountingRecordTypeId).HasColumnName(Constants.AccountingRecord.TypeId);
            
        }
    }

}
