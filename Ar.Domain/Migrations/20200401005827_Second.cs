﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ar.Domain.Migrations
{
    public partial class Second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_quote_users_UserId",
                table: "quote");

            migrationBuilder.DropIndex(
                name: "IX_quote_UserId",
                table: "quote");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "quote");

            migrationBuilder.UpdateData(
                table: "quote",
                keyColumn: "quote_id",
                keyValue: 1,
                columns: new[] { "DateExpire", "DateIssued" },
                values: new object[] { new DateTime(2020, 4, 30, 21, 58, 26, 756, DateTimeKind.Local).AddTicks(2205), new DateTime(2020, 3, 31, 21, 58, 26, 756, DateTimeKind.Local).AddTicks(1098) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "quote",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "quote",
                keyColumn: "quote_id",
                keyValue: 1,
                columns: new[] { "DateExpire", "DateIssued" },
                values: new object[] { new DateTime(2020, 4, 30, 21, 31, 42, 327, DateTimeKind.Local).AddTicks(6380), new DateTime(2020, 3, 31, 21, 31, 42, 327, DateTimeKind.Local).AddTicks(5886) });

            migrationBuilder.CreateIndex(
                name: "IX_quote_UserId",
                table: "quote",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_quote_users_UserId",
                table: "quote",
                column: "UserId",
                principalTable: "users",
                principalColumn: "user_id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
