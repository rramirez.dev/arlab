﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ar.Domain.Migrations
{
    public partial class First : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "quote",
                keyColumn: "quote_id",
                keyValue: 1,
                columns: new[] { "DateExpire", "DateIssued" },
                values: new object[] { new DateTime(2020, 4, 23, 0, 39, 55, 32, DateTimeKind.Local).AddTicks(1038), new DateTime(2020, 3, 24, 0, 39, 55, 32, DateTimeKind.Local).AddTicks(592) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "quote",
                keyColumn: "quote_id",
                keyValue: 1,
                columns: new[] { "DateExpire", "DateIssued" },
                values: new object[] { new DateTime(2020, 4, 23, 0, 38, 22, 82, DateTimeKind.Local).AddTicks(769), new DateTime(2020, 3, 24, 0, 38, 22, 82, DateTimeKind.Local).AddTicks(326) });
        }
    }
}
