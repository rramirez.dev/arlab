import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/WeatherForecasts';

const api = 'https://localhost:5001/api';

class FetchData extends Component {

  getQuotes() {
      fetch(api+'/quotes/all')
          .then(function (response) {
              return response.json();
          })
          .then(function (myJson) {
              console.log(myJson);
          });
  }

  componentDidMount() {
    // This method is called when the component is first added to the document
      this.ensureDataFetched();

  }

  componentDidUpdate() {
    // This method is called when the route parameters change
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
    this.props.requestWeatherForecasts(startDateIndex);
  }

  render() {
    return (
      <div>
        <h2> Quotes </h2>
            <div>
            <button type="button" className="btn btn-primary" onClick={this.getQuotes}> Get all </button>
            <button type="button" className="btn btn-primary"> Get by id </button>
            <button type="button" className="btn btn-primary"> Remove by id </button>
            <button type="button" className="btn btn-primary"> Save quote </button>
            <button type="button" className="btn btn-primary"> Update quote </button>
            <button type="button" className="btn btn-primary"> Patch quote </button>
        </div>
        {renderForecastsTable(this.props)}
        {renderPagination(this.props)}
      </div>
    );
  }
}

function renderForecastsTable(props) {
  return (
    <table className='table table-striped'>
      <thead>
        <tr>
          <th>Quote Id</th>
          <th>Total Price</th>
          <th>Analysis Id</th>
        </tr>
      </thead>
      <tbody>
        {props.forecasts.map(forecast =>
          <tr key={forecast.dateFormatted}>
            <td>{forecast.dateFormatted}</td>
            <td>{forecast.temperatureC}</td>
            <td>{forecast.temperatureF}</td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

function renderPagination(props) {
  const prevStartDateIndex = (props.startDateIndex || 0) - 5;
  const nextStartDateIndex = (props.startDateIndex || 0) + 5;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/fetch-data/${prevStartDateIndex}`}>Previous</Link>
    <Link className='btn btn-default pull-right' to={`/fetch-data/${nextStartDateIndex}`}>Next</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
  state => state.weatherForecasts,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(FetchData);
