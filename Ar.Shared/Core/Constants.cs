﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Ar.Shared.Core
{
    //Tables
    public partial class Constants
    {
        public class Tables
        {
            public const string AccountingRecord = "accounting_record";
            public const string AccountingRecordType = "accounting_record_type";
            public const string Analysis = "analysis";
            public const string AnalysisType = "analysis_type";
            public const string AnalysisParameter = "analysis_parameter";
            public const string Request = "request";
            public const string RequestItem = "request_item";
            public const string Organization = "organization";
            public const string Quote = "quote";
            public const string QuoteItem = "quote_item";
            public const string DeliverStatus = "deliver_status";
            public const string User = "users";
            public const string AnalysisTypeParameterRelationship = "analysis_type_parameter_relationship";
        }
    }

    //Fields
    public partial class Constants
    {
        public class Shared
        {
            public const string AnalysisId = "analysis_id";
            public const string UserId = "user_id";
            public const string OrganizationId = "organization_id";
            public const string Description = "description";
            public const string DeliverStatusId = "deliver_status_id";
        }

        public class AccountingRecord
        {
            public const string Id = "accounting_record_id";
            public const string StatusId = "deliver_status_id";
            public const string UserId = "user_id";
            public const string OrganizationId = "organization_id";
            public const string TypeId = "accounting_type_id";
            public const string TypeDescription = "accounting_record_type_description";
        }

        public class AccountingRecordType
        {
            public const string Id = "accounting_record_type_id";
            public const string Description = "accounting_record_type_description";
        }

        public class AnalysisParameter
        {
            public const string Id = "analysis_parameter_id";
            public const string Description = "analysis_parameter_description";
        }

        public class Deliver
        {
            public const string Description = "deliver_status_description";
        }

        public class Organization
        {
            public const string Name = "organization_name";
        }

        public class Quote
        {
            public const string Id = "quote_id";
            public const string TotalPrice = "quote_total_price";
        }

        public class QuoteItem
        {
            public const string Id = "quote_item_id";
            public const string Price = "quote_item_price";
            public const string Description = "quote_item_description";
        }

        public class Request
        {
            public const string Id = "request_id";
            public const string OriginDate = "request_origin_date";
            public const string DeliverDate = "request_deliver_date";
        }

        public class RequestItem
        {
            public const string Id = "request_item_id";
        }

        public class Analysis
        {
            public const string Id = "analysis_id";
            public const string Description = "analysis_description";
        }

        public class AnalysisParameterTypeRelationship
        {
            public const string Id = "analysis_type_parameter_relationship_id";
        }

        public class AnalysisType
        {
            public const string Id = "analysis_type_id"; 
            public const string Description = "analysis_type_description";
        }

        public class User
        {
            public const string Pass = "user_pass";
            public const string FirstName = "user_first_name";
            public const string LastName = "user_last_name";
            public const string Email = "user_email";
            public const string IsBoss = "user_is_boss";
        }
    }

    public static class ConstantsHelperFunctions
    {
        public static List<string> GetFieldsFromClass(object obj)
        {
            var result = new List<string>();

            var fields = obj.GetType().GetFields();
            foreach (var field in fields)
                result.Add(field.GetValue(obj).ToString());

            return result.OrderBy(c => c).ToList();
        }
    }

    public static class TestConstants
    {
        public const string Category = "Category";
        public const string Integration = "Integration";
        public const string Unit = "Unit";
        public const string Functional = "Functional";
    }
}
